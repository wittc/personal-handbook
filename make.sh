#!/usr/bin/env bash

pandoc --variable=geometry:a4paper --table-of-contents handbook.md --output=handbook.pdf
