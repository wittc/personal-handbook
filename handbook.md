% Personal Handbook
% Christian Witt
% 2019-09-09

# Introduction

I want to have as most of my documents organized and in plain text files as possible. Therefore I came up with the folder structure and file content syntax descripted in the following chapters. Some of the ideas were copied/inspired from other projects.

# Books

Third-party books (free or purchased) are located under `${HOME}/books`. The books stored here have to follow an open standard (e.g. TXT or PDF) and have to be DRM-free. Other books are stored on specialized devices (e.g. E-Book Reader).

Self written book / longer texts should be treated like projects (see separate section).

# Career

Documents that belong to my personal career are located in the folder `${HOME}/doc/career`.

Each document that correspond to a running job application is located in a separate subfolder with naming scheme `YYYY-MM-DD_EMPLOYER_JOBTITLE`:

* `YYYY-MM-DD` represents the date of job application
* `EMPLOYER` represents the (probable) employer
* `JOBTITLE` represents the job title

As job application documents are exchanged, they should be located according to `Inbox` section. As soon as the job application is finished, the corresponding folder can be removed.

Job application independend documents like cv, certifications and generic covering letter are located in the root folder.

# Converting

Because not every day-to-day task can be completed with plain text files or have binary files as input, some of the plain text files are going to be converted/exported into other (binary) files. The binary files should be located near the plain text document.

# Finances

All information regarding finances are located in the folder `${HOME}/doc/finance`.

The accounting is done in the file `accounting.txt` containing all non-periodic transaction records. All periodic transactions and pre-defined accounts should be located in the file `definitions.txt` which should be included by the `accounting.txt` file. The syntax used inside these files is described by the ledger utility[^ledger-homepage] which can be used for report generation.

# Inbox

All scanned letters and other documents received are located in the folder `${HOME}/doc/inbox/YYYY` and follow the naming scheme `YYYY-MM-DD_SOURCE_TITLE.EXT`:

* `YYYY-MM-DD` represents the date of the document (e.g. receive date)
* `SOURCE` represents the source or sender of the file
* `TITLE` describes the content of the file
* `EXT` represents the file extension according to mimetype

# Meeting minutes

Meeting minutes are located under `${HOME}/doc/meetings` and follow the naming scheme `YYYY-MM-DD_TITLE.md`:

* `YYYY-MM-DD` represents the meeting date.
* `TITLE` represents the title of the meeting.

The content should follow the following template:

```
# Attendees

* Person A
* Person B

# Agenda

1. first topic
```

The template is located at `${HOME}/doc/meetings/0000-00-00_TEMPLATE.md`.

# Notes

Notes are located under `${HOME}/doc/notes` and should use Markdown format[^markdown-format]. The file name is used as note title (excluding file extension).

# Projects

For tasks or projects that involve long-living documents like source code or a handbook there should be a project folder be created under `${HOME}/projects`. Each project folder have to be under version control and should have a meaningful name.

# Slides

Slides are located under `${HOME}/doc/slides` and follow the following template:

```
# Slide title

Some text

---

# Second slide title

Some more text
```

The template is located at `${HOME}/doc/slides/TEMPLATE.md`. Each slide can use Markdown format [^markdown-format] without the three dashes as these are reserved for slide splitting. The file name should represent the title of the presentation.

To convert this Markdown files into PDF slides, use `pandoc -t beamer`.

# Temporary files

All files which are short-living and doesn't fit into any of the described structures are stored in `${HOME}/tmp`.

# To-Do's

To-Do's are saved in a `todo.txt`[^todotxt-homepage] file which is located under `${HOME}/doc/notes`.
A single line in the `todo.txt` file represents a single task:

![todo.txt syntax description][todotxt-syntax-description]

## Usage of priorities

* **A**: The task I'm working on right now (for the event of interruption).
* **B**: Tasks that I plan to do today.
* **C**: Tasks that I plan to do this week.
* **D**: Tasks reserved for next week.
* **E-Z**: Not currently used.

Tasks without a priority are unscheduled.

# Tools

Used tools for specific tasks:

* diagram creator: PlantUML[^plantuml-homepage]
* document converter: pandoc[^pandoc-homepage]
* feed reader: newsboat[^newsboat-homepage]
* financial reports: ledger[^ledger-homepage]
* image viewer: imv[^imv-homepage]
* password manager: pass[^pass-homepage]
* PDF viewer: MuPDF[^mupdf-homepage]
* terminal emulator: termite[^termite-homepage]
* text editor: vim[^vim-homepage]
* version control: git[^git-homepage]
* video player: mpv[^mpv-homepage]
* web browser: firefox[^firefox-homepage]

[^imv-homepage]: [https://github.com/eXeC64/imv](https://github.com/eXeC64/imv)
[^firefox-homepage]: [https://firefox.com](https://firefox.com)
[^git-homepage]: [https://git-scm.com](https://git-scm.com)
[^ledger-homepage]: [https://www.ledger-cli.org](https://www.ledger-cli.org)
[^markdown-format]: [https://www.markdownguide.org](https://www.markdownguide.org)
[^mpv-homepage]: [https://mpv.io](https://mpv.io)
[^mupdf-homepage]: [https://mupdf.com](https://mupdf.com)
[^newsboat-homepage]: [https://newsboat.org](https://newsboat.org)
[^pandoc-homepage]: [https://pandoc.org](https://pandoc.org)
[^pass-homepage]: [https://www.passwordstore.org](https://www.passwordstore.org)
[^plantuml-homepage]: [http://plantuml.com](http://plantuml.com)
[^termite-homepage]: [https://github.com/thestinger/termite](https://github.com/thestinger/termite)
[^todotxt-homepage]: [http://todotxt.org](http://todotxt.org)
[^vim-homepage]: [https://www.vim.org](https://www.vim.org)

[todotxt-syntax-description]: todotxt-syntax-description.png "todo.txt syntax per line"
